// gulp (ayyyy lmao) 
var gulp = require('gulp');

// plugins
var jshint = require('gulp-jshint'),
    sass = require('gulp-sass'),
    prefix = require('gulp-autoprefixer'),
    cssmin = require('gulp-cssmin')
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    rename = require('gulp-rename'),
    svg2png = require('gulp-svg2png'),
    imageopt = require('gulp-image-optimization');
    require('gulp-grunt')(gulp, {
        base: null,
        prefix: 'grunt-',
        verbose: true
    });

// Startup tasks
// NOTICE: Runs Gruntfile.js
// ** Please update the Gruntfile.js settings
// ** when initializing a project
gulp.task('startup', function(){
    gulp.run('grunt-open');
});

// JSLint task
gulp.task('lint', function(){
    return gulp.src('build/js/*.js')
        .pipe(jshint())
        .pipe(jshint.reporter('default'));
});

// Prefix and minify CSS
gulp.task('styles', function(){
    return gulp.src('build/css/*.scss')
        .pipe(sass())
        .pipe(prefix('last 2 version', 'ie 9'))
        .pipe(cssmin())
        .pipe(rename('global.min.css'))
        .pipe(gulp.dest('css'))
});

// Minify JS
// !! PLEASE ADD a source-ordered concat function that
// !! ideally doesn't output an extra file to minify
gulp.task('scripts', function(){
    return gulp.src('build/js/global.js')
        .pipe(rename('global.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('js'));
});

// Create PNG fallbacks from SVG images
gulp.task('svg2png', function(){
    return gulp.src('img/*.svg')
        .pipe(svg2png())
        .pipe(gulp.dest('img'));
});

// Minify raster images
gulp.task('imageopt', function(){
    return gulp.src(['img/*.png', 'img/*.jpg', 'img/*.gif'])
        .pipe(imageopt({
            progressive: true,
            interlaced: true,
            optimizationLevel: 5
        }))
        .pipe(gulp.dest('img'));
});

// Task to handle the image tasks
gulp.task('images', ['svg2png', 'imageopt']);

// Build task - minify and concat all the things!
gulp.task('build', ['lint', 'styles', 'scripts', 'images']);

// Watch files for changes
gulp.task('watch', function(){
    gulp.watch('build/js/*.js', ['lint', 'scripts']);
    gulp.watch('build/css/*.scss', ['styles']);
    gulp.watch('style.css', ['build']);
    gulp.watch('img/*.svg', ['images']);
});

// Default task
gulp.task('default', ['startup', 'lint', 'styles', 'scripts', 'watch']);
gulp.task('restart', ['lint', 'styles', 'scripts', 'watch']);