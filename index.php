<?php get_header(); ?>

<section id="content" role="main">

<?php if ( have_posts() ) {

    if ( ! is_singular() ) echo '<ul class="loop-post-list">';

    while ( have_posts() ) {

        the_post();
        get_template_part('entry');
        comments_template();

    }

    if ( ! is_singular() ) echo '</ul>';

}?> 

<?php get_template_part('nav', 'below'); ?>

</section>

<?php get_footer(); ?>