<?php
/**
 * THEME INITIALIZATION
 */
add_action('after_setup_theme', 'blankslate_setup');
function blankslate_setup() {
    add_theme_support('automatic-feed-links');
    add_theme_support('post-thumbnails');

    global $content_width;
    if ( ! isset( $content_width ) ) $content_width = 640;
    register_nav_menus(
        array( 'main-menu' => __( 'Main Menu', 'blankslate' ) )
    );
}

// Set default the_title attribute for posts where the title was left blank.
add_filter('the_title', 'blankslate_title');
function blankslate_title($title) {
    if ($title == '') {
        return '&rarr;';
    } else {
        return $title;
    }
}

/**
 * STYLES AND SCRIPTS
 */

// // Enqueue jQuery
// if (!is_admin()) add_action("wp_enqueue_scripts", "my_jquery_enqueue", 11);
// function my_jquery_enqueue() {
//    wp_deregister_script('jquery');
//    wp_register_script('jquery', "http" . ($_SERVER['SERVER_PORT'] == 443 ? "s" : "") . "://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js", false, null, true);
//    wp_enqueue_script('jquery');
// }

// Enqueue Theme scripts and styles
if (!is_admin()) add_action("wp_enqueue_scripts", "br_enqueue_scripts");
function br_enqueue_scripts() {
    /* Scripts --- */

    // wp_register_script('global-js', get_template_directory_uri() . '/js/global.min.js', array('jquery'), null, true);
    // wp_enqueue_script('global-js');

    /* Styles --- */

    wp_register_style('global-css', get_template_directory_uri() . '/css/global.min.css', false, null);
    wp_enqueue_style('global-css');
}

/**
 * WORDPRESS ADMIN CUSTOMIZATIONS
 */

// Remove that unnecessary admin bar from the front end
add_filter('show_admin_bar', '__return_false');

// Allow SVG and other file types to be uploaded as media
add_filter( 'upload_mimes', 'cc_mime_types' );
function cc_mime_types( $mimes ){
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}

// // Remove menus from admin dashboard for non Administrator accounts
// function remove_menus () {
//     global $menu;

//     // add top level menus here
//     $restricted = array( __('Comments'), __('Tools'), __('SEO'));
    
//     end ($menu);
//     while (prev($menu)){
//       $value = explode(' ',$menu[key($menu)][0]);
//       if(in_array($value[0] != NULL?$value[0]:"" , $restricted)){unset($menu[key($menu)]);}
//     }
// }
// if ( !current_user_can('create_users') ) { add_action('admin_menu', 'remove_menus'); }

// // Remove submenu items from admin dashboard for non Admins
// function remove_submenus() {
//   $page = remove_submenu_page('index.php?page=relevanssi', 'relevanssi.php');;
// }
// add_action('admin_menu', 'remove_submenus', 999);

// // Remove Yoast SEO meta box from edit posts
// function remove_seo_meta() {
//   remove_meta_box('wpseo_meta', 'post', 'normal');
//   remove_meta_box('wpseo_meta', 'page', 'normal');
//   remove_meta_box('wpseo_meta', 'trainings', 'normal');
//   remove_meta_box('wpseo_meta', 'resources', 'normal');
//   remove_meta_box('wpseo_meta', 'training_questions', 'normal');
//   remove_meta_box('wpseo_meta', 'staff', 'normal');
//   remove_meta_box('wpseo_meta', 'audits', 'normal');
// }
// if ( !current_user_can('create_users') ) add_action('add_meta_boxes', 'remove_seo_meta', 99);

// // Add custom taxonomies and custom post types counts to dashboard
// add_action( 'dashboard_glance_items', 'my_add_cpt_to_dashboard' );
// function my_add_cpt_to_dashboard() {
//   $showTaxonomies = 0;
//   // Custom taxonomies counts
//   if ($showTaxonomies) {
//     $taxonomies = get_taxonomies( array( '_builtin' => false ), 'objects' );
//     foreach ( $taxonomies as $taxonomy ) {
//       $num_terms  = wp_count_terms( $taxonomy->name );
//       $num = number_format_i18n( $num_terms );
//       $text = _n( $taxonomy->labels->singular_name, $taxonomy->labels->name, $num_terms );
//       $associated_post_type = $taxonomy->object_type;
//       if ( current_user_can( 'manage_categories' ) ) {
//         $output = '<a href="edit-tags.php?taxonomy=' . $taxonomy->name . '&post_type=' . $associated_post_type[0] . '">' . $num . ' ' . $text .'</a>';
//       }
//       echo '<li class="taxonomy-count">' . $output . ' </li>';
//     }
//   }
//   // Custom post types counts
//   $post_types = get_post_types( array( '_builtin' => false ), 'objects' );
//   foreach ( $post_types as $post_type ) {
//     if($post_type->show_in_menu==false) {
//       continue;
//     }
//     $num_posts = wp_count_posts( $post_type->name );
//     $num = number_format_i18n( $num_posts->publish );
//     $text = _n( $post_type->labels->singular_name, $post_type->labels->name, $num_posts->publish );
//     if ( current_user_can( 'edit_posts' ) ) {
//         $output = '<a href="edit.php?post_type=' . $post_type->name . '">' . $num . ' ' . $text . '</a>';
//     }
//     echo '<li class="page-count ' . $post_type->name . '-count">' . $output . '</td>';
//   }
// }

// // Add Report a Bug / BitBucket Issue Tracking Widget to Dashboard
// function issue_tracker_dashboard_widget() {

//   wp_add_dashboard_widget(
//    'issue-tracker-widget',         // Widget slug.
//    'Submit a Support Request',     // Title.
//    'issue_tracker_widget_function' // Display function.
//   );  
// }
// add_action( 'wp_dashboard_setup', 'issue_tracker_dashboard_widget' );

// Issue Tracking Widget Contents
/* function issue_tracker_widget_function() { ?>

  <?php if ( isset($_POST['save']) ) {
    // Get User Info
    global $current_user;
    get_currentuserinfo();

    // Get message data
    $type = $_POST['request-type'];
    $subject = '['. $type .'] '. stripslashes(trim($_POST['title']));
    $message = stripslashes(trim($_POST['content']));
    $headers = 'From: '. $current_user->user_login .' <'. $current_user->user_email .'>';

    if ( mail( 'support@thatbrightrobot.com', $subject, $message, $headers ) ) {
      echo 'Success! We\'ve received your message and we\'ll get back to you as soon as possible.';
    } else {
      echo 'Sorry, there was a problem sending your message. Try emailing support@thatbrightrobot.com or calling us at (303) 832-2384.';
    }

  } else { ?>

    <p>Having trouble editing the site? Found a bug that needs to be fixed? Let us know and we'll make sure you're taken care of.</p>

    <form name="issue-tracker" action="<?php echo admin_url(); ?>" method="post" class="initial-form hide-if-no-js">

      <div class="select-wrap" id="select-wrap">
        <label for="request-type" class="prompt">What type of request is this?</label>
        <select name="request-type" id="request-type">
          <option value="HELP">Help Request</option>
          <option value="BUG">Bug Fix</option>
        </select>
      </div><p />
      
      <div class="input-text-wrap" id="title-wrap">
        <label class="prompt" for="title" id="title-prompt-text">Subject</label>
        <input type="text" name="title" id="title" autocomplete="off">
      </div>

      <div class="textarea-wrap" id="description-wrap">
        <label class="prompt" for="content" id="content-prompt-text">What’s the problem?</label>
        <textarea name="content" id="content" class="mceEditor" rows="3" cols="15" autocomplete="off" style="overflow: hidden; height: 33px; resize: vertical;"></textarea>
      </div>

      <p class="submit">
        <input type="submit" name="save" id="save-post" class="button button-primary" value="Send Issue">
        <br class="clear">
      </p>

    </form>

  <?php } ?>

<?php } */

// // Remove unecessary dashboard widgets
// function example_remove_dashboard_widget() {
//   remove_meta_box( 'dashboard_quick_press', 'dashboard', 'side' );
//   remove_meta_box( 'dashboard_primary', 'dashboard', 'side' );
//   remove_meta_box( 'dashboard_browser_nag', 'dashboard', 'normal' );
//   remove_meta_box( 'dashboard_activity', 'dashboard', 'normal' );
// }
// add_action('wp_dashboard_setup', 'example_remove_dashboard_widget' );

/**
 * TinyMCE Editor Customizations
 */

// // Add buttons that are normally disabled
// function my_mce_buttons_2($buttons) {
//   $buttons[] = 'superscript';
//   $buttons[] = 'subscript';

//   return $buttons;
// }
// add_filter('mce_buttons_2', 'my_mce_buttons_2');

// // Remove styles from the visual editor
// function remove_mce_styles($arr) {
//   // First check which screen
//   $curr_screen = get_current_screen();
//   if ( $curr_screen->post_type !== 'resources' ) {
//     $arr['block_formats'] = 'Paragraph=p;Heading 3=h3;Heading 4=h4;Heading 5=h5;Heading 6=h6';
//     return $arr;
//   } else {
//     $arr['block_formats'] = 'Paragraph=p;Heading 1=h1;Heading 2=h2;Heading 3=h3;Heading 4=h4;Heading 5=h5;Heading 6=h6';
//     return $arr;
//   }
// }
// add_filter('tiny_mce_before_init', 'remove_mce_styles');

/**
 * THEME FUNCTIONS
 */

// // Backend Favicon
// add_action('admin_head', 'show_favicon');
// function show_favicon() {
//   echo '<link href="' . get_bloginfo('template_url') . '/img/cu-logo.png" rel="icon" type="image/x-icon">';
// }

// // Rename the "Enter Title Here" bar for a custom post type.
// function change_default_title( $title ){
 
//   $cpt = 'CPT-SLUG-HERE';
//   $new_text = 'ENTER NEW TEXT HERE';
//   $screen = get_current_screen();

//   if ( $cpt == $screen->post_type ){
//       $title = 'Enter question here';
//   }

//   if ( 'staff' == $screen->post_type ) {
//     $title = $new_text;
//   }

//   return $title;
// }
 
add_filter( 'enter_title_here', 'change_default_title' );