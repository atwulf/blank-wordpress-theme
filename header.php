<!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>
        <?php wp_title( ' | ', true, 'right' ); if ( is_home() || is_front_page() ) { echo ' | '; bloginfo('description'); } ?>
    </title>
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
    <header id="header" role="banner">
        <?php echo '<'. ( is_singular() ? 'h2' : 'h1' ) .' id="site-title">'; ?>
        <a href="<?php echo esc_url(home_url('/')); ?>" title="<?php esc_attr_e(get_bloginfo('name')); ?>" rel="home">
            <?php echo esc_html(get_bloginfo('name')); ?>
        </a>
        <?php echo '</'. ( is_singular() ? 'h2' : 'h1' ) .'>'; ?>

        <div id="site-description">
            <?php bloginfo('description'); ?>
        </div>

        <nav id="menu" role="navigation">
            <?php wp_nav_menu( array( 'theme_location' => 'main-menu' ) ); ?>
        </nav>
    </header>