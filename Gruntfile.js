module.exports = function(grunt){

  grunt.initConfig({
    open : {
      local : {
        path: 'http://localhost:8888/ckliger',
        app: 'Google Chrome'
      },
      stage : {
        path : 'http://dev.ckliger.thatbrightrobot.com/wp-admin',
        app: 'Google Chrome'
      },
      git : {
        path : '/Applications/MAMP/htdocs/ckliger/wp-content/themes/ckliger',
        app: 'Terminal'
      },
      live : {
          path: 'http://www.carolkliger.com',
          app: 'Google Chrome'
      }
    }
  })

  grunt.loadNpmTasks('grunt-open');

};