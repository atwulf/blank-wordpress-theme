<?php if ( is_singular() ) { ?>

    <?php // Single post page architecture ?>
    <article class="single-post">

        <span class="entry-date"><?php echo get_the_date('F jS, Y'); ?></span>
        
        <h1 class="entry-title">
            <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" rel="bookmark"><?php the_title(); ?></a>
        </h1>

        <?php the_content(); ?>

        <?php get_template_part('nav-below', 'single'); ?>

    </article>

<?php } else { ?>

    <?php // Loop post architecture ?>
    <li class="loop-post">

        <span class="entry-date"><?php echo get_the_date('F jS, Y'); ?></span>

        <h2 class="entry-title">
            <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" rel="bookmark"><?php the_title(); ?></a>
        </h2>

        <?php the_excerpt(); ?>

    </li>

<?php } ?>